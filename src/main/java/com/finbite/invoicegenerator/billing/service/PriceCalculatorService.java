package com.finbite.invoicegenerator.billing.service;

import static com.finbite.invoicegenerator.billing.model.AmountType.*;
import static com.finbite.invoicegenerator.common.model.ErrorCode.PACKAGE_NOT_FOUND;

import com.finbite.invoicegenerator.billing.model.Usage;
import com.finbite.invoicegenerator.billing.model.UsageAmount;
import com.finbite.invoicegenerator.billing.model.UsageAmountDetail;
import com.finbite.invoicegenerator.common.exception.ApplicationException;
import com.finbite.invoicegenerator.plan.exception.PackageDataException;
import com.finbite.invoicegenerator.plan.model.ExtraUsagePrice;
import com.finbite.invoicegenerator.plan.model.Package;
import com.finbite.invoicegenerator.plan.service.PackageService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
@Slf4j
public class PriceCalculatorService {

    private final PackageService packageService;

    public UsageAmountDetail calculateUsageAmount(Usage usage) {
        final Package servicePackage = findPackage(usage.getPackageName());
        log.info("package found for name %s, package: %s".formatted(usage.getPackageName(), servicePackage));

        final ExtraUsagePrice extraUsagePrice = getExtraUsage();
        log.info("extraUsagePrice info: %s".formatted(extraUsagePrice));

        final List<UsageAmount> usageAmounts = new ArrayList();
        usageAmounts.add(getBasePackageAmount(servicePackage));
        usageAmounts.add(getCallsAmount(servicePackage, extraUsagePrice, usage));
        usageAmounts.add(getSmsAmount(servicePackage, extraUsagePrice, usage));

        return buildUsageAmountDetail(servicePackage, usageAmounts);
    }

    private UsageAmount getBasePackageAmount(Package servicePackage) {
        return UsageAmount.builder()
                .amountType(BASE)
                .amount(servicePackage.getCost())
                .total(servicePackage.getCost())
                .build();
    }

    private UsageAmount getCallsAmount(Package servicePackage, ExtraUsagePrice extraUsagePrice, Usage usage) {
        return UsageAmount.builder()
                .amountType(CALL)
                .excessiveQuantity(getExtraUnits(usage.getUsedCalls(), servicePackage.getCalls()))
                .amount(extraUsagePrice.getCallAmount())
                .total(getExtraUnits(usage.getUsedCalls(), servicePackage.getCalls()) * extraUsagePrice.getCallAmount())
                .build();
    }

    private UsageAmount getSmsAmount(Package servicePackage, ExtraUsagePrice extraUsagePrice, Usage usage) {
        return UsageAmount.builder()
                .amountType(SMS)
                .excessiveQuantity(getExtraUnits(usage.getUsedSms(), servicePackage.getSms()))
                .amount(extraUsagePrice.getSmsAmount())
                .total(getExtraUnits(usage.getUsedSms(), servicePackage.getSms()) * extraUsagePrice.getSmsAmount())
                .build();
    }

    private UsageAmountDetail buildUsageAmountDetail(Package servicePackage, List<UsageAmount> usageAmounts) {
        return UsageAmountDetail.builder()
                .servicePackage(servicePackage)
                .usageAmounts(usageAmounts)
                .total(usageAmounts.stream()
                        .mapToLong(UsageAmount::getTotal)
                        .sum())
                .build();
    }

    private Package findPackage(String packageName) {
        Package servicePackage;
        try {
            servicePackage= packageService.getPackage(packageName);
        } catch (PackageDataException packageDataException) {
            log.error(packageDataException.getMessage());
            throw new ApplicationException(PACKAGE_NOT_FOUND, packageDataException.getMessage());
        }
        return servicePackage;
    }

    private ExtraUsagePrice getExtraUsage() {
        return packageService.getExtraUsagePrice();
    }

    private int getExtraUnits(int usageUnits, int includedUnits) {
        return Math.max(0, usageUnits - includedUnits);
    }
}
