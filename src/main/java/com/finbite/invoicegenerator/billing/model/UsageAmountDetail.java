package com.finbite.invoicegenerator.billing.model;

import com.finbite.invoicegenerator.plan.model.Package;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class UsageAmountDetail {
    private Package servicePackage;
    private List<UsageAmount> usageAmounts;
    private Long total;
}
