package com.finbite.invoicegenerator.billing.model;

public enum AmountType {
    BASE, CALL, SMS
}
