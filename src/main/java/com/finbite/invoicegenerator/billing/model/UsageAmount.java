package com.finbite.invoicegenerator.billing.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class UsageAmount {
    private AmountType amountType;
    private int excessiveQuantity;
    private Long amount;
    private Long total;
}
