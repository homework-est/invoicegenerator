package com.finbite.invoicegenerator.billing.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Usage {
    private String packageName;
    private int usedCalls;
    private int usedSms;
}
