package com.finbite.invoicegenerator.utils;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import static java.time.LocalDate.now;

public class DateUtil {

    public static final String DATE_FORMAT = "dd.MM.yyyy";

    public static String getCurrentDate() {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(DATE_FORMAT);
        return now().format(formatter);
    }

}
