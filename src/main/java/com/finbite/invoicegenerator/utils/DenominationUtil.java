package com.finbite.invoicegenerator.utils;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class DenominationUtil {

  public static BigDecimal centsToEuros(Long cents) {
    return new BigDecimal(Double.toString(cents * 0.01)).setScale(2, RoundingMode.HALF_EVEN);
  }

  public static BigDecimal convertNullSafeCentsToEuros(Long cents) {
    if(cents == null) {
      return BigDecimal.ZERO;
    } else {
      return centsToEuros(cents);
    }
  }
}
