package com.finbite.invoicegenerator.utils;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

public class Utils {

  public static String getNullSafeString(Object object) {
    return  Optional.ofNullable(object).orElseGet(()-> "").toString();
  }

  public static <T> Stream<T> streamOf(List<T> list) {
    return Optional.ofNullable(list).orElse(Collections.emptyList()).stream();
  }

  public static boolean isNonNegativeInteger(String input) {
    try {
      int value = Integer.parseInt(getNullSafeString(input));
      return value >= 0;
    } catch (NumberFormatException e) {
      return false;
    }
  }
}
