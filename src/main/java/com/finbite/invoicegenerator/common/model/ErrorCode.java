package com.finbite.invoicegenerator.common.model;

public enum ErrorCode {
    PACKAGE_NOT_FOUND, INTERNAL_ERROR, VALIDATION_ERROR
}
