package com.finbite.invoicegenerator;

import com.finbite.invoicegenerator.gateway.service.InvoiceGeneratorService;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.Scanner;

@SpringBootApplication
@RequiredArgsConstructor
public class InvoicegeneratorApplication implements CommandLineRunner {

	public static void main(String[] args) {
		SpringApplication.run(InvoicegeneratorApplication.class, args);
	}

	private final InvoiceGeneratorService invoiceGeneratorService;

	@Override
	public void run(String... args) {
		invoiceGeneratorService.initiateInvoiceProcess();
	}


	//@Override
	public void run11(String... args) {
		var scanner = new Scanner(System.in);
		String controlString;
		do {
			System.out.println("Generating invoice...");
			// Simulate invoice generation
			System.out.println("To continue, enter any key. To stop, type 'quit'.");
			controlString = scanner.nextLine().toLowerCase();
		} while (!controlString.equalsIgnoreCase("quit"));
	}
}
