package com.finbite.invoicegenerator.plan.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.finbite.invoicegenerator.plan.exception.PackageDataException;
import com.finbite.invoicegenerator.plan.model.ExtraUsagePrice;
import com.finbite.invoicegenerator.plan.model.Package;
import com.finbite.invoicegenerator.plan.model.ServicePackages;
import jakarta.annotation.PostConstruct;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.Optional;

import static com.finbite.invoicegenerator.utils.Utils.*;

@Service
@RequiredArgsConstructor
@Slf4j
public class PackageService {

    private static final String PLAN_FILE_NAME = "plans.json";

    private List<Package> packages;
    private ExtraUsagePrice extraUsagePrice;
    private final ObjectMapper objectMapper;

    @PostConstruct
    public void init() throws IOException {
        loadPackagesFromConfig();
    }

    private void loadPackagesFromConfig() throws IOException {
        log.info("reading config file starts");
        byte[] jsonData = Files.readAllBytes(Paths.get(PLAN_FILE_NAME));
        ServicePackages servicePackages = objectMapper.readValue(jsonData, ServicePackages.class);
        packages = servicePackages.getPackages();
        log.info("packages:%s".formatted(packages));
        extraUsagePrice = servicePackages.getExtraUsagePrice();
        log.info("extraUsagePrice:%s".formatted(extraUsagePrice));
    }

    public Package getPackage(String packageName) {
        return streamOf(packages)
                .filter(p -> p.getName().equalsIgnoreCase(packageName.toLowerCase()))
                .findFirst()
                .orElseThrow(() -> new PackageDataException("Package not found with name: %s".formatted(packageName)));
    }

    public ExtraUsagePrice getExtraUsagePrice() {
        return Optional.ofNullable(extraUsagePrice)
                .orElseThrow(() -> new PackageDataException("Package extra usage data not found"));

    }
}
