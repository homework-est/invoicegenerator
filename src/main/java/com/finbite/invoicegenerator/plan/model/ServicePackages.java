package com.finbite.invoicegenerator.plan.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;


@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ServicePackages {

    private List<Package> packages;
    private ExtraUsagePrice extraUsagePrice;
}
