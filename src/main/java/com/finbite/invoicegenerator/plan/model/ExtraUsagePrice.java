package com.finbite.invoicegenerator.plan.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ExtraUsagePrice {
    private Long callAmount;
    private Long smsAmount;
}
