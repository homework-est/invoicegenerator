package com.finbite.invoicegenerator.plan.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Package {
    private String name;
    private int calls;
    private int sms;
    private Long cost;
}
