package com.finbite.invoicegenerator.plan.exception;

import lombok.Getter;

@Getter
public class PackageDataException extends RuntimeException {

    public PackageDataException(String message) {
        super(message);
    }

    public PackageDataException(String message, Throwable cause) {
        super(message, cause);
    }
}
