package com.finbite.invoicegenerator.gateway.service.validation;

import com.finbite.invoicegenerator.gateway.service.validation.event.InputCapturedEvent;

public interface Validator {

    void validate(InputCapturedEvent inputCapturedEvent);
}
