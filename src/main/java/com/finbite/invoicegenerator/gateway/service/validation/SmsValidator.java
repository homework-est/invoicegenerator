package com.finbite.invoicegenerator.gateway.service.validation;

import com.finbite.invoicegenerator.common.exception.ApplicationException;
import com.finbite.invoicegenerator.gateway.service.validation.event.InputCapturedEvent;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import static com.finbite.invoicegenerator.common.model.ErrorCode.VALIDATION_ERROR;
import static com.finbite.invoicegenerator.utils.Utils.isNonNegativeInteger;

@Component
@Slf4j
public class SmsValidator implements Validator {

    @Override
    public void validate(InputCapturedEvent inputCapturedEvent) {
        String sms = (String) inputCapturedEvent.getValue();
        if(!isNonNegativeInteger(sms)) {
            log.error("Validation error for sms. input:%s".formatted(sms));
            throw new ApplicationException(VALIDATION_ERROR, "SMSes must be non negative number");
        }
    }
}
