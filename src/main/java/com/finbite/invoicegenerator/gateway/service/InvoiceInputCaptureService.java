package com.finbite.invoicegenerator.gateway.service;

import com.finbite.invoicegenerator.billing.model.Usage;
import com.finbite.invoicegenerator.gateway.service.validation.event.InputCapturedEvent;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;

import java.util.Scanner;

import static com.finbite.invoicegenerator.gateway.model.InputType.*;

@Service
@RequiredArgsConstructor
@Slf4j
public class InvoiceInputCaptureService {

    private final ApplicationEventPublisher eventPublisher;

    public Usage captureInvoiceInput() {
        var scanner = new Scanner(System.in);

        System.out.println("Enter package type (Small, Medium, Large):");
        String packageName = scanner.nextLine().toLowerCase();
        eventPublisher.publishEvent(new InputCapturedEvent<>(PACKAGE_NAME, packageName));

        System.out.println("Enter actual minutes used:");
        String callsUsage = scanner.nextLine();
        eventPublisher.publishEvent(new InputCapturedEvent<>(CALL, callsUsage));

        System.out.println("Enter actual SMS used:");
        String smsUsage = scanner.nextLine();
        eventPublisher.publishEvent(new InputCapturedEvent<>(SMS, smsUsage));

        log.info("invoice input process completed");
        return Usage.builder()
                .packageName(packageName)
                .usedCalls(Integer.parseInt(callsUsage))
                .usedSms(Integer.parseInt(smsUsage))
                .build();
    }
}
