package com.finbite.invoicegenerator.gateway.service.validation.listener;

import com.finbite.invoicegenerator.gateway.model.InputType;
import com.finbite.invoicegenerator.gateway.service.validation.CallValidator;
import com.finbite.invoicegenerator.gateway.service.validation.PackageNameValidator;
import com.finbite.invoicegenerator.gateway.service.validation.SmsValidator;
import com.finbite.invoicegenerator.gateway.service.validation.Validator;
import com.finbite.invoicegenerator.gateway.service.validation.event.InputCapturedEvent;
import jakarta.annotation.PostConstruct;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

@Component
@RequiredArgsConstructor
@Slf4j
public class InputCapturedEventListener {

    private final PackageNameValidator packageNameValidator;
    private final CallValidator callValidator;
    private final SmsValidator smsValidator;

    private static Map<InputType, Validator> validatorMap;

    @PostConstruct
    public void initValidatorMap() {
        validatorMap = new HashMap<>();
        validatorMap.put(InputType.PACKAGE_NAME, packageNameValidator);
        validatorMap.put(InputType.CALL, callValidator);
        validatorMap.put(InputType.SMS, smsValidator);
    }

    @EventListener
    public <T> void handleInputCapturedEvent(InputCapturedEvent<T> event) {
        log.info("input captured type: %s, value: %s".formatted(event.getInputType(), event.getValue()));

        if(Optional.ofNullable(validatorMap.get(event.getInputType())).isPresent()) {
            validatorMap.get(event.getInputType()).validate(event);
        }
    }
}
