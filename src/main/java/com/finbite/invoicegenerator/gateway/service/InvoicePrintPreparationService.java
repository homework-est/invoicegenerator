package com.finbite.invoicegenerator.gateway.service;

import com.finbite.invoicegenerator.billing.model.AmountType;
import com.finbite.invoicegenerator.billing.model.UsageAmount;
import com.finbite.invoicegenerator.billing.model.UsageAmountDetail;
import com.finbite.invoicegenerator.common.exception.ApplicationException;
import com.finbite.invoicegenerator.printing.model.ErrorData;
import com.finbite.invoicegenerator.printing.model.InvoiceData;
import com.finbite.invoicegenerator.printing.service.InvoicePrintingService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

import static com.finbite.invoicegenerator.billing.model.AmountType.CALL;
import static com.finbite.invoicegenerator.billing.model.AmountType.SMS;
import static com.finbite.invoicegenerator.common.model.ErrorCode.INTERNAL_ERROR;
import static com.finbite.invoicegenerator.utils.DateUtil.getCurrentDate;
import static com.finbite.invoicegenerator.utils.DenominationUtil.convertNullSafeCentsToEuros;
import static com.finbite.invoicegenerator.utils.Utils.streamOf;

@Service
@RequiredArgsConstructor
@Slf4j
public class InvoicePrintPreparationService {

    private static final String INVOICE_NUMBER_PREFIX = "INV-";

    private final InvoicePrintingService invoicePrintingService;

    public void printInvoice(UsageAmountDetail usageAmountDetail) {
        log.info("calculation information, usageAmountDetail:%s".formatted(usageAmountDetail));
        InvoiceData invoiceData = getInvoiceData(usageAmountDetail);
        String invoice = invoicePrintingService.generateInvoice(invoiceData);
        System.out.println(invoice);
    }

    public void printError(ErrorData errorData) {
        log.info("Error in process, errorData:%s".formatted(errorData));
        String error = invoicePrintingService.generateErrorMessage(errorData);
        System.out.println(error);
    }

    private InvoiceData getInvoiceData(UsageAmountDetail usageAmountDetail) {
        UsageAmount callsUsageAmount = getUsageAmount(usageAmountDetail.getUsageAmounts(), CALL);
        UsageAmount smsUsageAmount = getUsageAmount(usageAmountDetail.getUsageAmounts(), SMS);

        return InvoiceData.builder()
                .invoiceNumber(INVOICE_NUMBER_PREFIX + UUID.randomUUID()) //can be improved to have more meaningful invoice number
                .date(getCurrentDate())
                .packageName(usageAmountDetail.getServicePackage().getName())
                .packageCost(convertNullSafeCentsToEuros(usageAmountDetail.getServicePackage().getCost()))
                .extraCalls(callsUsageAmount.getExcessiveQuantity())
                .extraCallRate(convertNullSafeCentsToEuros(callsUsageAmount.getAmount()))
                .totalExtraCallAmount(convertNullSafeCentsToEuros(callsUsageAmount.getTotal()))
                .extraSms(smsUsageAmount.getExcessiveQuantity())
                .extraSmsRate(convertNullSafeCentsToEuros(smsUsageAmount.getAmount()))
                .totalExtraSmsAmount(convertNullSafeCentsToEuros(smsUsageAmount.getTotal()))
                .total(convertNullSafeCentsToEuros(usageAmountDetail.getTotal()))
                .build();
    }

    private UsageAmount getUsageAmount(List<UsageAmount> usageAmounts, AmountType amountType) {
       return streamOf(usageAmounts)
               .filter(usageAmount -> usageAmount.getAmountType().equals(amountType))
               .findFirst()
               .orElseThrow(() -> new ApplicationException(INTERNAL_ERROR, "Usage Amount is missing for %s".formatted(amountType)));
    }
}
