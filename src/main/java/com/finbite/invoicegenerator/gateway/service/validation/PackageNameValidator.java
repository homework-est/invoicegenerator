package com.finbite.invoicegenerator.gateway.service.validation;

import com.finbite.invoicegenerator.common.exception.ApplicationException;
import com.finbite.invoicegenerator.gateway.service.validation.event.InputCapturedEvent;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import static com.finbite.invoicegenerator.common.model.ErrorCode.VALIDATION_ERROR;
import static io.micrometer.common.util.StringUtils.isBlank;

@Component
@Slf4j
public class PackageNameValidator implements Validator {

    @Override
    public void validate(InputCapturedEvent inputCapturedEvent) {
        String packageName = (String) inputCapturedEvent.getValue();
        if(isBlank(packageName)) {
            log.error("Validation error for packageName. input:%s".formatted(packageName));
            throw new ApplicationException(VALIDATION_ERROR, "Package name cannot be blank");
        }
    }
}
