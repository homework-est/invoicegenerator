package com.finbite.invoicegenerator.gateway.service;

import com.finbite.invoicegenerator.billing.model.Usage;
import com.finbite.invoicegenerator.billing.model.UsageAmountDetail;
import com.finbite.invoicegenerator.billing.service.PriceCalculatorService;
import com.finbite.invoicegenerator.common.exception.ApplicationException;
import com.finbite.invoicegenerator.printing.model.ErrorData;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Scanner;

@Service
@RequiredArgsConstructor
@Slf4j
public class InvoiceGeneratorService {

    private final InvoiceInputCaptureService invoiceInputCaptureService;
    private final InvoicePrintPreparationService invoicePrintPreparationService;
    private final PriceCalculatorService priceCalculatorService;

    public void initiateInvoiceProcess() {
        var scanner = new Scanner(System.in);

        String controlString;
        do {
            log.info("starting the invoice generation process");
            generateInvoice();
            System.out.println("To continue enter any key, to stop type 'quit'");
            controlString = scanner.nextLine().toLowerCase();

        } while(!controlString.equalsIgnoreCase("quit"));
    }

    public void generateInvoice() {
        try {
            log.info("invoice data input process begins");
            Usage usage = invoiceInputCaptureService.captureInvoiceInput();

            log.info("calculation process begins");
            UsageAmountDetail usageAmountDetail = priceCalculatorService.calculateUsageAmount(usage);

            log.info("printing invoice information process begins");
            invoicePrintPreparationService.printInvoice(usageAmountDetail);
        } catch (ApplicationException ae) {
            log.error("error in generating invoice: %s".formatted(ae.getMessage()));
            var errorData = ErrorData.builder()
                    .errorCode(ae.getErrorCode())
                    .message(ae.getMessage())
                    .build();
            invoicePrintPreparationService.printError(errorData);
        }
    }
}
