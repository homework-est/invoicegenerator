package com.finbite.invoicegenerator.gateway.service.validation.event;

import com.finbite.invoicegenerator.gateway.model.InputType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class InputCapturedEvent<T> {
    private InputType inputType;
    private T value;
}
