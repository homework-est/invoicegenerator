package com.finbite.invoicegenerator.gateway.model;

public enum InputType {
    PACKAGE_NAME, CALL, SMS
}
