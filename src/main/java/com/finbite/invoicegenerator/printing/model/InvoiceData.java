package com.finbite.invoicegenerator.printing.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class InvoiceData {

    private String invoiceNumber;
    private String date;
    private String packageName;
    private BigDecimal packageCost;
    private int extraCalls;
    private BigDecimal extraCallRate;
    private BigDecimal totalExtraCallAmount;
    private int extraSms;
    private BigDecimal extraSmsRate;
    private BigDecimal totalExtraSmsAmount;
    private BigDecimal total;
}
