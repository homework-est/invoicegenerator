package com.finbite.invoicegenerator.printing.model;

import com.finbite.invoicegenerator.common.model.ErrorCode;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ErrorData {

    private ErrorCode errorCode;
    private String message;
}
