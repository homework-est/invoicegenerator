package com.finbite.invoicegenerator.printing.service;

import org.springframework.stereotype.Service;

@Service
public class TemplateService {

    public String getInvoiceTemplate() {
        return  "----------------------------------\n" +
                "           INVOICE                \n" +
                "----------------------------------\n" +
                "Invoice Number                 : %s\n" +
                "Date                           : %s\n" +
                "Package Name                   : %s\n" +
                "Package cost                   : %s Euro\n" +
                "Extra calls (%d X %s Euro)     : %s Euro\n" +
                "Extra SMS (%d X %s Euro)       : %s Euro\n" +
                "Total                          : %s Euro\n" +
                "----------------------------------\n";
    }

    public String getErrorTemplate() {
        return  "----------------------------------\n" +
                "             ERROR                \n" +
                "----------------------------------\n" +
                "Error Code                : %s\n" +
                "Message                   : %s\n" +
                "----------------------------------\n";
    }
}
