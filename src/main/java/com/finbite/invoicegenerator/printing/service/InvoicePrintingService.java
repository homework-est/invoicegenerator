package com.finbite.invoicegenerator.printing.service;

import com.finbite.invoicegenerator.printing.model.ErrorData;
import com.finbite.invoicegenerator.printing.model.InvoiceData;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Slf4j
public class InvoicePrintingService {

    private final TemplateService templateService;

    public String generateInvoice(InvoiceData invoiceData) {
        log.info("Displaying invoice information.");
        return templateService.getInvoiceTemplate().formatted(
                invoiceData.getInvoiceNumber(),
                invoiceData.getDate(),
                invoiceData.getPackageName(),
                invoiceData.getPackageCost(),
                invoiceData.getExtraCalls(),
                invoiceData.getExtraCallRate(),
                invoiceData.getTotalExtraCallAmount(),
                invoiceData.getExtraSms(),
                invoiceData.getExtraSmsRate(),
                invoiceData.getTotalExtraSmsAmount(),
                invoiceData.getTotal());
    }

    public String generateErrorMessage(ErrorData errorData) {
        log.info("Displaying error information.");
        return templateService.getErrorTemplate().formatted(errorData.getErrorCode(), errorData.getMessage());
    }
}
