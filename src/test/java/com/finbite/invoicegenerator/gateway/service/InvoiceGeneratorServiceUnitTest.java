package com.finbite.invoicegenerator.gateway.service;

import static org.mockito.Mockito.*;

import com.finbite.invoicegenerator.billing.model.Usage;
import com.finbite.invoicegenerator.billing.model.UsageAmountDetail;
import com.finbite.invoicegenerator.billing.service.PriceCalculatorService;
import com.finbite.invoicegenerator.common.exception.ApplicationException;
import com.finbite.invoicegenerator.common.model.ErrorCode;
import com.finbite.invoicegenerator.printing.model.ErrorData;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

public class InvoiceGeneratorServiceUnitTest {

    private static final String PACKAGE_NAME = "small";

    @Mock
    private InvoiceInputCaptureService invoiceInputCaptureService;

    @Mock
    private PriceCalculatorService priceCalculatorService;

    @Mock
    private InvoicePrintPreparationService invoicePrintPreparationService;

    @InjectMocks
    private InvoiceGeneratorService invoiceGeneratorService;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    public void testGenerateInvoice_Success() {
        // assume
        final var usage = Usage.builder()
                .packageName(PACKAGE_NAME)
                .usedCalls(20)
                .usedSms(100)
                .build();

        final var usageAmountDetail = UsageAmountDetail.builder()
                .total(1000L)
                .build();

        // given
        when(invoiceInputCaptureService.captureInvoiceInput()).thenReturn(usage);
        when(priceCalculatorService.calculateUsageAmount(any(Usage.class))).thenReturn(usageAmountDetail);

        // when
        invoiceGeneratorService.generateInvoice();

        // then
        verify(invoiceInputCaptureService, times(1)).captureInvoiceInput();
        verify(priceCalculatorService, times(1)).calculateUsageAmount(any(Usage.class));
        verify(invoicePrintPreparationService, times(1)).printInvoice(any(UsageAmountDetail.class));
        verify(invoicePrintPreparationService, never()).printError(any(ErrorData.class));
    }

    @Test
    public void testGenerateInvoice_Failure() {
        // assume
        final var usage = Usage.builder()
                .packageName(PACKAGE_NAME)
                .usedCalls(20)
                .usedSms(100)
                .build();

        ApplicationException exception = new ApplicationException(ErrorCode.PACKAGE_NOT_FOUND, "Package not found");

        // given
        when(invoiceInputCaptureService.captureInvoiceInput()).thenReturn(usage);
        when(priceCalculatorService.calculateUsageAmount(any(Usage.class))).thenThrow(exception);

        // when
        invoiceGeneratorService.generateInvoice();

        // then
        verify(invoiceInputCaptureService, times(1)).captureInvoiceInput();
        verify(priceCalculatorService, times(1)).calculateUsageAmount(any(Usage.class));
        verify(invoicePrintPreparationService, never()).printInvoice(any(UsageAmountDetail.class));
        verify(invoicePrintPreparationService, times(1)).printError(any(ErrorData.class));
    }
}
