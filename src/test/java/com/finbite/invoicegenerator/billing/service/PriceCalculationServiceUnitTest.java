package com.finbite.invoicegenerator.billing.service;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

import com.finbite.invoicegenerator.billing.model.Usage;
import com.finbite.invoicegenerator.billing.model.UsageAmountDetail;
import com.finbite.invoicegenerator.common.exception.ApplicationException;
import com.finbite.invoicegenerator.plan.exception.PackageDataException;
import com.finbite.invoicegenerator.plan.model.ExtraUsagePrice;
import com.finbite.invoicegenerator.plan.model.Package;
import com.finbite.invoicegenerator.plan.service.PackageService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

public class PriceCalculationServiceUnitTest {

    private static final String PACKAGE_NAME = "small";
    private static final Long PACKAGE_COST = 500L; //cents
    private static final int PACKAGE_INCLUDED_CALLS = 10;
    private static final int PACKAGE_INCLUDED_SMS = 50;
    private static final Long EXTRA_USAGE_CALL_RATE = 20L; //cents
    private static final Long EXTRA_USAGE_SMS_RATE = 30L; //cents

    @Mock
    private PackageService packageService;

    @InjectMocks
    private PriceCalculatorService priceCalculatorService;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    public void testCalculateUsageAmount_Success() {
        // assume
        final var servicePackage = Package.builder()
                .name(PACKAGE_NAME)
                .cost(PACKAGE_COST)
                .calls(PACKAGE_INCLUDED_CALLS)
                .sms(PACKAGE_INCLUDED_SMS)
                .build();

        final var extraUsagePrice = ExtraUsagePrice.builder()
                .callAmount(EXTRA_USAGE_CALL_RATE)
                .smsAmount(EXTRA_USAGE_SMS_RATE)
                .build();

        final var usage = Usage.builder()
                .packageName(PACKAGE_NAME)
                .usedCalls(20)
                .usedSms(100)
                .build();


        // given
        when(packageService.getPackage(PACKAGE_NAME)).thenReturn(servicePackage);
        when(packageService.getExtraUsagePrice()).thenReturn(extraUsagePrice);

        // when
        UsageAmountDetail usageAmountDetail = priceCalculatorService.calculateUsageAmount(usage);

        // then
        assertNotNull(usageAmountDetail);
        assertThat(usageAmountDetail.getServicePackage().getCost(), is(equalTo(PACKAGE_COST)));
        assertThat(usageAmountDetail.getServicePackage().getName(), is(equalTo(PACKAGE_NAME)));
        assertThat(usageAmountDetail.getServicePackage().getCalls(), is(equalTo(PACKAGE_INCLUDED_CALLS)));
        assertThat(usageAmountDetail.getServicePackage().getSms(), is(equalTo(PACKAGE_INCLUDED_SMS)));

        assertThat(usageAmountDetail.getTotal(), is(equalTo(2200L)));
    }

    @Test
    public void testCalculateUsageAmount_PackageNotFound() {
        // assume
        final var usage = Usage.builder()
                .packageName(PACKAGE_NAME)
                .usedCalls(20)
                .usedSms(100)
                .build();

        // given
        when(packageService.getPackage(PACKAGE_NAME)).thenThrow(new PackageDataException("Package not found"));

        // then
        ApplicationException exception = assertThrows(ApplicationException.class, () -> {
            priceCalculatorService.calculateUsageAmount(usage);
        });

        assertEquals("Package not found", exception.getMessage());
        verify(packageService, times(1)).getPackage(PACKAGE_NAME);
        verify(packageService, never()).getExtraUsagePrice();
    }

    @Test
    public void testCalculateUsageAmount_ExtraUsagePriceFailure() {
        // assume
        final var servicePackage = Package.builder()
                .name(PACKAGE_NAME)
                .cost(PACKAGE_COST)
                .calls(PACKAGE_INCLUDED_CALLS)
                .sms(PACKAGE_INCLUDED_SMS)
                .build();

        final var usage = Usage.builder()
                .packageName(PACKAGE_NAME)
                .usedCalls(20)
                .usedSms(100)
                .build();

        // given
        when(packageService.getPackage(PACKAGE_NAME)).thenReturn(servicePackage);
        when(packageService.getExtraUsagePrice()).thenThrow(new RuntimeException("Failed to retrieve extra usage price"));

        // then
        RuntimeException exception = assertThrows(RuntimeException.class, () -> {
            priceCalculatorService.calculateUsageAmount(usage);
        });

        assertEquals("Failed to retrieve extra usage price", exception.getMessage());
        verify(packageService, times(1)).getPackage(PACKAGE_NAME);
        verify(packageService, times(1)).getExtraUsagePrice();
    }
}
