# invoicegenerator



## Getting started

It is command line application to generate invoice. It takes package information from plans.json file.
In plans.json file all amounts are presented in cents.

This application can be improved to divide into microservices by creating separate services from packages plan, gateway, billing and printing.
The essential unit tests are written but more can be written in future.

logs will be found under logs folder.

## Build and Test

To build the application following command can be used.

```
gradle clean build
```

To package the application following command can be used.

```
./gradlew build
```

Once the package is made. Navigate to build/libs folder and locate the jar file. Make sure to have plans.json file is present in same folder.
(The plans.json file is present on project root folder)

Now to run the application use following command. You may change the jar file name as per generated jar file name

```
java -jar invoicegenerator-0.0.1-SNAPSHOT.jar
```

Now you can follow the instructions present on command prompt to generate invoice.